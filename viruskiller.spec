Summary: A Shoot 'Em Up that uses your file system
Name: %{name}
Version: %{version}
Release: %{release}
Source: %{name}-%{version}-%{release}.tar.gz
Vendor: Parallel Realities
Packager: Stephen Sweeney
URL: http://www.parallelrealities.co.uk/projects/virusKiller.php
Group: Games/Arcade
License: GPL
%description
A virus killing simulation, that makes use of your own file system

%prep
%setup -q

%build
make VERSION=%{version} RELEASE=%{release}

%install
make install

%clean

%post


%files
%dir /usr/games
%dir /usr/share/games/viruskiller
%dir /usr/share/doc/%{name}
%dir /usr/share/applnk
/usr/games/%{name}
/usr/share/games/viruskiller/%{name}.pak
/usr/share/doc/%{name}/*
/usr/share/applications/viruskiller.desktop
/usr/share/icons/hicolor/16x16/apps/viruskiller.png
/usr/share/icons/hicolor/32x32/apps/viruskiller.png
/usr/share/icons/hicolor/64x64/apps/viruskiller.png
