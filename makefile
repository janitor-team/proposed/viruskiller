PROG = viruskiller
PAKNAME = viruskiller.pak
DOCS = doc/*
ICONS = icons/

VERSION = 1.02
RELEASE = 1
USEPAK = 1

PREFIX=$(DESTDIR)/usr
BINDIR = $(PREFIX)/games/
DATADIR = $(PREFIX)/share/games/$(PROG)/
DOCDIR = $(PREFIX)/share/doc/$(PROG)/
ICONDIR = $(PREFIX)/share/icons/hicolor/
DESKTOPDIR = $(PREFIX)/share/applications/
MEDAL_SERVER_HOST = www.parallelrealities.co.uk
#MEDAL_SERVER_HOST = localhost
MEDAL_SERVER_PORT = 80

SAFEDIR = /tmp/

CFLAGS += `sdl-config --cflags` -DVERSION=$(VERSION) -DRELEASE=$(RELEASE) -DUSEPAK=$(USEPAK)
CFLAGS += -DPAKNAME=\"$(PAKNAME)\" -DPAKLOCATION=\"$(DATADIR)\" -DSAFEDIR=\"$(SAFEDIR)\" -DUNIX -DGAMEPLAYMANUAL=\"$(DOCDIR)manual.html\" -Wall
CFLAGS += -DMEDAL_SERVER_HOST=\"$(MEDAL_SERVER_HOST)\" -DMEDAL_SERVER_PORT=$(MEDAL_SERVER_PORT)
LIBS = `sdl-config --libs` -lSDL_mixer -lSDL_image -lSDL_ttf -lSDL_net

OBJS += CAudio.o 
OBJS += CBase.o
OBJS += CCollision.o 
OBJS += CData.o CDirectory.o 
OBJS += CEngine.o 
OBJS += CFile.o CFileData.o 
OBJS += CGameData.o CGameObject.o CGraphics.o 
OBJS += CHighScore.o 
OBJS += CItem.o 
OBJS += CList.o 
OBJS += CMath.o CMedalServer.o
OBJS += CPak.o CParticle.o
OBJS += CSprite.o 
OBJS += CVirus.o 
OBJS += CWidget.o

OBJS += filesAndDirectories.o 
OBJS += game.o
OBJS += init.o items.o 
OBJS += highscores.o 
OBJS += main.o 
OBJS += particles.o
OBJS += resources.o 
OBJS += title.o 
OBJS += viruses.o 
OBJS += widgets.o

PAKOBJS = CFileData.o pak.o

# top-level rule to create the program.
all: $(PROG) pak

# compiling other source files.
%.o: src/%.cpp src/%.h src/defs.h src/headers.h
	$(CXX) $(CFLAGS) -c $<

# linking the program.
$(PROG): $(OBJS)
	$(CXX) $(LIBS) $(OBJS) -o $(PROG)
	
pak: $(PAKOBJS)
	$(CXX) $(LIBS) $(PAKOBJS) -o pak

# cleaning everything that can be automatically recreated with "make".
clean:
	$(RM) $(OBJS)

buildpak:
	./pak data gfx music sound $(PAKNAME)

# install
install:

	./pak data gfx music sound $(PAKNAME)

	mkdir -p $(BINDIR)
	mkdir -p $(DATADIR)
	mkdir -p $(DOCDIR)
	mkdir -p $(ICONDIR)16x16/apps
	mkdir -p $(ICONDIR)32x32/apps
	mkdir -p $(ICONDIR)64x64/apps
	mkdir -p $(DESKTOPDIR)

	install -o root -g games -m 755 $(PROG) $(BINDIR)$(PROG)
	install -o root -g games -m 644 $(PAKNAME) $(DATADIR)$(PAKNAME)
	install -o root -g games -m 644 $(DOCS) $(DOCDIR)
	cp $(ICONS)$(PROG).png $(ICONDIR)32x32/apps/
	cp $(ICONS)$(PROG)-mini.png $(ICONDIR)16x16/apps/$(PROG).png
	cp $(ICONS)$(PROG)-large.png $(ICONDIR)64x64/apps/$(PROG).png
	cp $(ICONS)$(PROG).desktop $(DESKTOPDIR)

uninstall:
	$(RM) $(BINDIR)$(PROG)
	$(RM) $(DATADIR)$(PAKNAME)
	$(RM) -r $(DOCDIR)
	$(RM) $(ICONDIR)$(ICONS)$(PROG).png
	$(RM) $(ICONDIR)16x16/apps/$(PROG).png
	$(RM) $(ICONDIR)32x32/apps/$(PROG).png
	$(RM) $(ICONDIR)64x64/apps/$(PROG).png
	$(RM) $(DESKTOPDIR)$(PROG).desktop
	